module Model exposing (Model, init)

import Url exposing (Url)
import Browser.Navigation exposing (Key)

type alias Model =
    {}


init : () -> Url -> Key -> (Model, Cmd msg)
init _ _ _ =
    (Model, Cmd.none)

