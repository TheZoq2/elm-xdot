module View exposing (view)

import Browser exposing (Document)

import Model exposing (Model)
import Msg exposing (Msg)

view : Model -> Document Msg
view model = Document "graph viewer" []


