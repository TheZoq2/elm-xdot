module Main exposing (..)

import Browser

import Model exposing (Model, init)
import View exposing (view)
import Msg exposing (Msg)


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    (model, Cmd.none)


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = (\_ -> Sub.none)
        , onUrlChange = (\_ -> ())
        , onUrlRequest = (\_ -> ())
        }
